---
title: "Maxwell Systems"
weight: 60
description: "Maxwell Systems"
---

Since version 10, {{< octopus >}} can also be used to calculate Maxwell systems, i.e. propagating the electro-magnetic fields.
In future versions, this will be coupled to matter systems, to go beyond the usual forward-only coupling between the fields and the electrons.

{{< tutorial-series "Maxwell" >}}