---
Title: "Creating geometries"
tutorials: "Maxwell_details"
Weight: 100
---


### Creating a geometry for the linear medium

Octopus uses the [CGAL](https://www.cgal.org) library, which allows to parse
standard geometry files, as used e.g. for 3D printing. Currently, we support
only the [OFF](https://en.wikipedia.org/wiki/OFF_(file_format)) file format.

There are many software packages around to generate such files, but we
recommend [openSCAD](https://www.openscad.org/index.html) and `ctmconv` which
is part of [OpenCTM](https://openctm.sourceforge.net/).

{{< notice info >}}
    In Debian style systems, these can be installed using
    {{< code-block >}}
    apt-get install openscad
    apt-get install openctm-tools
    {{< /code-block >}}
{{< /notice >}}

OpenSCAD provides its own scripting language for creating geometries, which can
be created in a GUI, but also can be rendered on the command line.  The
documentation of OpenSCAD can be found
[here](https://www.openscad.org/documentation.html).

{{< expand "Example: creating a simple lens with Openscad" >}}
<br>
Here is an example script for a lens, generated from the intersection of two spheres:
{{< code-block >}}
$fs=0.5;
$fa=0.5;
intersection(){
  translate([-22,0,0]) sphere(r=25);
  translate([ 22,0,0]) sphere(r=25);
};
{{< /code-block >}}
For details on the syntax of this, see the OpenSCAD {{< link "https://www.openscad.org/documentation.html" "tutorials" >}}.

This creates the following lens: {{< figure src="/images/Maxwell/Lense.png" width="500px" >}}

{{< /expand >}}

If you copy the above code into a file `lens.scad`, you can generate the required `OFF` file by the command:
```bash
openscad -o lens.off lens.scad
```

{{< notice warning >}}
    The `OFF` files, generated by openSCAD are malformed. If you want to further manipulate them with `ctmconv`, the header needs to be fixed:
    {{< code-block >}}
    OFF 1162 2320 0
    1.81312 4.79828 5.90861
    1.73416 3.80751 6.86969
    1.98506 3.84776 5.90861
    1.72673 7.81373 -0.993913
    1.39732 8.75085 -0.993913
    1.41584 8.75777 0
    ...
    {{< /code-block >}}
    The first line needs to be `OFF` only, and the first line of the data block needs to contain the number of vertices, planes and (optional) connections.
    You can add comments (starting with `#` of empty lines are allowed after the header).
    {{< code-block >}}
    OFF
    # This is a lens (generated with OpenSCAD)

    1162 2320 0
    1.81312 4.79828 5.90861
    1.73416 3.80751 6.86969
    1.98506 3.84776 5.90861
    1.72673 7.81373 -0.993913
    1.39732 8.75085 -0.993913
    1.41584 8.75777 0
    ...
    {{< /code-block >}}
{{< /notice >}}




