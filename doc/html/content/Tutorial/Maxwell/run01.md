---
Title: "Cosinoidal plane wave in vacuum"
tutorials: "Maxwell"
Weight: 11
---

## Cosinoidal plane wave in vacuum

In this tutorial, we will describe the propagation of a cosinoidal wave in vacuum.

{{% expand "click for full input file" %}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp
{{< /code-block >}}
{{% /expand %}}

{{% notice warning %}}
  Currently, this run only works with 1 or 2 MPI tasks.
{{% /notice %}}

First, define the calculation mode and the parallelization strategy:

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp calc_mode
{{< /code-block >}}


The total size of the (physical) simulation box is 10.0 x 10.0 x 10.0 Bohr with
a spacing of 0.5 Bohr in each direction. As discussed in the {{< tutorial
"maxwell/simulationbox" "section on simulation boxes" >}}, also the boundary
points have to be accounted for. Hence the total size of the simulation box is
chosen to be 12.0 x 12.0 x 12.0 Bohr.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp box
{{< /code-block >}}

Since we propagate without a linear medium, we can use the vacuum Maxwell
Hamiltonian operator. Derivatives and exponential expansion order are four.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp calculation
{{< /code-block >}}

The boundary conditions are chosen as plane waves to simulate the incoming wave
without any absorption at the boundaries.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp boundaries
{{< /code-block >}}

The simulation time step is set to 0.002 and the system propagates to a maximum
time step of 200. For each time step, we write an output of the electric and
the magnetic field as well as the energy density, the total Maxwell energy.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp timestep
{{< /code-block >}}

The incident cosinoidal plane wave passes the simulation box, where the
cosinoidal pulse has a width of 10.0 Bohr, a spatial shift of 25.0 Bohr in the
negative x-direction, an amplitude of 0.05 a.u., and a wavelength of 10.0 Bohr.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp field
{{< /code-block >}}


Finally, the output options are set:

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/1.1_pulse_td/inp output
{{< /code-block >}}

Contour plot of the electric field in z-direction after 50 time steps for
t=0.11 and 100 time steps for t=0.21:

{{% expand "Example gnuplot script" %}}
```bash
set pm3d
set view map
set palette defined (-0.1 "blue", 0 "white", 0.1 "red")
set term png size 1000,500

unset surface
unset key

set output 'plot.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-0.1:0.1]

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.105328 au)'
sp [-10:10][-10:10][-0.1:0.1] 'Maxwell/output_iter/td.0000050/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.210656 au)'
sp [-10:10][-10:10][-0.1:0.1] 'Maxwell/output_iter/td.0000100/e_field-z.z=0' u 1:2:3

unset multiplot
```
If you copy this script into a file called {{< file "plot.gnu" >}}, you can create the plots by:
```bash
gnuplot plot.gnu
```
{{% /expand %}}

{{< figure src="/images/Maxwell/tutorial_01_run_electric_field_contour.png" width="50%" >}}


Maxwell fields at the origin and Maxwell energy inside the free Maxwell
propagation region of the simulation box:

{{% expand "gnuplot script" %}}
```bash
set term png size 1000,400
set output 'plot2.png'

set multiplot
set style data l

set xlabel 'time [a.u.]'

set origin 0.025,0
set size 0.45,0.9
set title 'Electric and magnetic fields'
set ylabel 'Maxwell fields [a.u.]'
p 'Maxwell/td.general/total_e_field_z' u 2:3 t 'E_z', 'Maxwell/td.general/total_b_field_y' u 2:($3*100) t '100 * B_y'

set origin 0.525,0
set size 0.45,0.9
set title 'Maxwell energy'
set ylabel 'Maxwell energy [a.u.]'
p  'Maxwell/td.general/maxwell_energy' u 2:3  t 'Maxwell energy'

unset multiplot

```
{{% /expand %}}

{{< figure src="/images/Maxwell/tutorial_01_run_maxwell_energy_and_fields.png" width="50%" >}}

{{< tutorial-footer >}}
